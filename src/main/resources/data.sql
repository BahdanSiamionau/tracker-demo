INSERT INTO person VALUES (1, 'Alice');
INSERT INTO person VALUES (2, 'Bob');
INSERT INTO person VALUES (3, 'Carol');

INSERT INTO project VALUES (1, 'Service A', 1);
INSERT INTO project VALUES (2, 'Service B', 2);

INSERT INTO task VALUES (1, 'Development', 1, 1);
INSERT INTO task VALUES (2, 'Testing', 1, 1);
INSERT INTO task VALUES (3, 'Fixing', 1, 3);
INSERT INTO task VALUES (4, 'Planning', 2, 2);

INSERT INTO record VALUES (1, 1, 1, parsedatetime('01-06-2016', 'dd-MM-yyyy'), 1);
INSERT INTO record VALUES (2, 1, 2, parsedatetime('02-06-2016', 'dd-MM-yyyy'), 2);
INSERT INTO record VALUES (3, 2, 3, parsedatetime('03-06-2016', 'dd-MM-yyyy'), 3);
INSERT INTO record VALUES (4, 3, 1, parsedatetime('04-06-2016', 'dd-MM-yyyy'), 3);
INSERT INTO record VALUES (5, 3, 2, parsedatetime('05-06-2016', 'dd-MM-yyyy'), 2);
INSERT INTO record VALUES (6, 4, 3, parsedatetime('06-06-2016', 'dd-MM-yyyy'), 1);
