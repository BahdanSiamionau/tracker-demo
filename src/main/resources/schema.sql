CREATE TABLE person (
	id BIGINT AUTO_INCREMENT,
	name VARCHAR,
	PRIMARY KEY (id)
);

CREATE TABLE project (
	id BIGINT AUTO_INCREMENT,
	name VARCHAR,
	person_id BIGINT,
	PRIMARY KEY (id)
);

CREATE TABLE task (
	id BIGINT AUTO_INCREMENT,
	name VARCHAR,
	project_id BIGINT,
	person_id BIGINT,
	PRIMARY KEY (id)
);

CREATE TABLE record (
	id BIGINT AUTO_INCREMENT,
	task_id BIGINT,
	person_id BIGINT,
	date TIMESTAMP,
	spent_time INTEGER,
	PRIMARY KEY (id)
);