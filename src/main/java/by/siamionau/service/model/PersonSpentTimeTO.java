package by.siamionau.service.model;

import by.siamionau.persistence.model.Person;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PersonSpentTimeTO {

	private Person person;
	private Integer spentTime;
}
