package by.siamionau.service;

import by.siamionau.persistence.model.Project;

import java.util.Set;

public interface ProjectService {

	Project getById(Long id);

	Set<Project> getAll();

	void save(Project project);
}
