package by.siamionau.service;

import by.siamionau.persistence.model.Task;

import java.util.Set;

public interface TaskService {

	Task getById(Long id);

	Set<Task> getAll();

	void save(Task task);
}
