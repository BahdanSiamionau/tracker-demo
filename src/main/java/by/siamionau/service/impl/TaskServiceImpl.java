package by.siamionau.service.impl;

import by.siamionau.persistence.model.Task;
import by.siamionau.persistence.repository.TaskRepository;
import by.siamionau.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class TaskServiceImpl implements TaskService {

	private TaskRepository taskRepository;

	@Override
	public Task getById(Long id) {
		return this.taskRepository.findOne(id);
	}

	@Override
	public Set<Task> getAll() {
		HashSet<Task> tasks = new HashSet<>();
		this.taskRepository.findAll().forEach(tasks::add);
		return tasks;
	}

	@Override
	public void save(Task task) {
		this.taskRepository.save(task);
	}

	@Autowired
	public void setTaskRepository(TaskRepository taskRepository) {
		this.taskRepository = taskRepository;
	}
}
