package by.siamionau.service.impl;

import by.siamionau.persistence.repository.PersonRepository;
import by.siamionau.persistence.repository.ReportRepository;
import by.siamionau.service.PersonService;
import by.siamionau.service.model.PersonSpentTimeTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PersonServiceImpl implements PersonService {

	private PersonRepository personRepository;
	private ReportRepository reportRepository;

	@Override
	public Set<PersonSpentTimeTO> getSpentTime(Date fromDate, Date toDate) {
		Set<PersonSpentTimeTO> result = new HashSet<>();
		Map<Long, Integer> reportData = reportRepository.spentTimeReport(fromDate, toDate);
		this.personRepository.findAll().forEach(p -> {
			Integer spentTime = reportData.get(p.getId());
			if (spentTime != null) {
				result.add(new PersonSpentTimeTO(p, spentTime));
			}
		});
		return result;
	}

	@Autowired
	public void setPersonRepository(PersonRepository personRepository) {
		this.personRepository = personRepository;
	}

	@Autowired
	public void setReportRepository(ReportRepository reportRepository) {
		this.reportRepository = reportRepository;
	}
}
