package by.siamionau.service.impl;

import by.siamionau.persistence.model.Project;
import by.siamionau.persistence.repository.ProjectRepository;
import by.siamionau.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class ProjectServiceImpl implements ProjectService {

	private ProjectRepository projectRepository;

	@Override
	public Project getById(Long id) {
		return this.projectRepository.findOne(id);
	}

	@Override
	public Set<Project> getAll() {
		HashSet<Project> projects = new HashSet<>();
		this.projectRepository.findAll().forEach(projects::add);
		return projects;
	}

	@Override
	public void save(Project project) {
		this.projectRepository.save(project);
	}

	@Autowired
	public void setProjectRepository(ProjectRepository projectRepository) {
		this.projectRepository = projectRepository;
	}
}
