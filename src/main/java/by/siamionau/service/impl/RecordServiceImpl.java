package by.siamionau.service.impl;

import by.siamionau.persistence.model.Record;
import by.siamionau.persistence.repository.RecordRepository;
import by.siamionau.service.RecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Service
public class RecordServiceImpl implements RecordService {

	private RecordRepository recordRepository;

	@Override
	public Record getById(Long id) {
		return this.recordRepository.findOne(id);
	}

	@Override
	public Set<Record> getByDateRange(Date fromDate, Date toDate) {
		return recordRepository.findRecordsBetweenDates(fromDate, toDate);
	}

	@Override
	public Set<Record> getAll() {
		HashSet<Record> records = new HashSet<>();
		this.recordRepository.findAll().forEach(records::add);
		return records;
	}

	@Override
	public void save(Record record) {
		this.recordRepository.save(record);
	}

	@Autowired
	public void setRecordRepository(RecordRepository recordRepository) {
		this.recordRepository = recordRepository;
	}
}
