package by.siamionau.service;

import by.siamionau.service.model.PersonSpentTimeTO;

import java.util.Date;
import java.util.Set;

public interface PersonService {

	Set<PersonSpentTimeTO> getSpentTime(Date fromDate, Date toDate);
}
