package by.siamionau.service;

import by.siamionau.persistence.model.Record;

import java.util.Date;
import java.util.Set;

public interface RecordService {

	Record getById(Long id);

	Set<Record> getByDateRange(Date fromDate, Date toDate);

	Set<Record> getAll();

	void save(Record record);
}
