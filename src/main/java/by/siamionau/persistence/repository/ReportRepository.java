package by.siamionau.persistence.repository;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ReportRepository {

	@Data
	public static class SpentTimeResultDTO {
		private Long personId;
		private Integer sumSpentTime;
	}

	private static final String SPENT_TIME_QUERY = "select sum(spent_time) sum_spent_time, person_id " +
			"from record where date between ? and ? group by person_id";

	private JdbcTemplate jdbcTemplate;

	public Map<Long, Integer> spentTimeReport(Date fromDate, Date toDate) {

		List<SpentTimeResultDTO> searchResults = jdbcTemplate.query(SPENT_TIME_QUERY,
				new BeanPropertyRowMapper<>(SpentTimeResultDTO.class),
				fromDate, toDate
		);

		HashMap<Long, Integer> reportResult = new HashMap<>();
		searchResults.forEach(sr -> reportResult.put(sr.getPersonId(), sr.getSumSpentTime()));
		return reportResult;
	}

	@Autowired
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
}
