package by.siamionau.persistence.repository;

import by.siamionau.persistence.model.Record;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.Set;

@Repository
public interface RecordRepository extends CrudRepository<Record, Long> {

	@Query("SELECT r FROM Record r where r.date between :fromDate and :toDate")
	Set<Record> findRecordsBetweenDates(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);
}
