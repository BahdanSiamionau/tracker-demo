package by.siamionau.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@EqualsAndHashCode(callSuper = true, exclude = {"records"})
@JsonIgnoreProperties({"project"})
public class Task extends AbstractEntity {

	private String name;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="projectId")
	private Project project;

	@ManyToOne
	@JoinColumn(name="personId")
	private Person owner;

	@OneToMany(mappedBy = "task", fetch = FetchType.EAGER)
	private Set<Record> records;

}
