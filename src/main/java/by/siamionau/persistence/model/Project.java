package by.siamionau.persistence.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@EqualsAndHashCode(callSuper = true, exclude = {"tasks"})
public class Project extends AbstractEntity {

	private String name;

	@ManyToOne
	@JoinColumn(name = "personId")
	private Person owner;

	@OneToMany(mappedBy = "project", fetch = FetchType.EAGER)
	private Set<Task> tasks;
}
