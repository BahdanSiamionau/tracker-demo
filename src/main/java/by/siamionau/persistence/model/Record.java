package by.siamionau.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties({"task"})
public class Record extends AbstractEntity {

	@Temporal(TemporalType.DATE)
	private Date date;
	private Integer spentTime;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="taskId")
	private Task task;

	@ManyToOne
	@JoinColumn(name="personId")
	private Person owner;
}
