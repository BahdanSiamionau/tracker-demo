package by.siamionau.web;

import by.siamionau.service.PersonService;
import by.siamionau.service.model.PersonSpentTimeTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Set;

@RestController
@RequestMapping("/person")
public class PersonController {

	private PersonService personService;

	@RequestMapping(method = RequestMethod.GET, path = "/report")
	public Set<PersonSpentTimeTO> getRecords(@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date fromDate,
											 @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date toDate) {
		return personService.getSpentTime(fromDate, toDate);
	}

	@Autowired
	public void setPersonService(PersonService personService) {
		this.personService = personService;
	}
}
