package by.siamionau.web;

import by.siamionau.persistence.model.Project;
import by.siamionau.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("/project")
public class ProjectController {

	private ProjectService projectService;

	@RequestMapping(method = RequestMethod.GET)
	public Set<Project> getProjects() {
		return projectService.getAll();
	}


	@RequestMapping(method = RequestMethod.GET, path = "/{id}")
	public Project getProject(@PathVariable Long id) {
		return projectService.getById(id);
	}

	@RequestMapping(method = RequestMethod.POST)
	public void saveProject(@RequestBody Project project) {
		projectService.save(project);
	}

	@Autowired
	public void setProjectService(ProjectService projectService) {
		this.projectService = projectService;
	}
}
