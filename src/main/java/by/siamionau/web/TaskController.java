package by.siamionau.web;

import by.siamionau.persistence.model.Task;
import by.siamionau.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("/task")
public class TaskController {

	private TaskService taskService;

	@RequestMapping(method = RequestMethod.GET)
	public Set<Task> getTasks() {
		return taskService.getAll();
	}


	@RequestMapping(method = RequestMethod.GET, path = "/{id}")
	public Task getTask(@PathVariable Long id) {
		return taskService.getById(id);
	}

	@RequestMapping(method = RequestMethod.POST)
	public void saveTask(@RequestBody Task task) {
		taskService.save(task);
	}

	@Autowired
	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}
}
