package by.siamionau.web;

import by.siamionau.persistence.model.Record;
import by.siamionau.service.RecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Set;

@RestController
@RequestMapping("/record")
public class RecordController {

	private RecordService recordService;

	@RequestMapping(method = RequestMethod.GET)
	public Set<Record> getRecords() {
		return recordService.getAll();
	}

	@RequestMapping(method = RequestMethod.GET, path = "/report")
	public Set<Record> getRecords(@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date fromDate,
								  @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date toDate) {
		return recordService.getByDateRange(fromDate, toDate);
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{id}")
	public Record getRecord(@PathVariable Long id) {
		return recordService.getById(id);
	}

	@RequestMapping(method = RequestMethod.POST)
	public void saveRecord(@RequestBody Record record) {
		recordService.save(record);
	}

	@Autowired
	public void setRecordService(RecordService recordService) {
		this.recordService = recordService;
	}
}
