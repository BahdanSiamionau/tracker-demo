Run
----------

To run application you need Java 8 installed on your box - access the application folder and run

> ./gradlew bootRun

You don't need anything else - database used for implementation is embedded H2

API
----------

List of available endpoints and methods:

1. GET/POST /project
2. GET /project/1
3. GET/POST /task
4. GET /task/1
5. GET/POST /record
6. GET /record/1
7. GET /person/report?fromDate=2016-06-01&toDate=2016-06-04
    Returns person list with sum of spent time in a given time interval
8. GET /record/report?fromDate=2016-06-04&toDate=2016-06-05
    Returns all worklog records for a given time interval
